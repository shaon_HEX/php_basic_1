<?php

// This is a one-line c++ style comment

?>
<?php

/* This is a multi line comment
       yet another line of comment */

?>


<?php

# This is a one-line shell-style comment
?>


<?php
//A nice way to toggle the commenting of blocks of code can be done by mixing the two comment styles
//*
if ($foo) {
  echo $bar;
}
// */
sort($morecode);
?>



<?php
//Now by taking out one / on the first line..
/*
if ($foo) {
  echo $bar;
}
// */
sort($morecode);
?>